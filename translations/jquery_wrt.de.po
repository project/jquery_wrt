# $Id$
# German translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
# jquery_wrt.admin.inc: n/a
# jquery_wrt.module: n/a
# jquery_wrt.info: n/a
# jquery_wrt.js: n/a
# Julian <Julian@Pustkuchen.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2013-03-29 19:52+0100\n"
"PO-Revision-Date: 2013-03-30 15:12+0200\n"
"Last-Translator: Julian <Julian@Pustkuchen.com>\n"
"Language-Team: webks: websolutions kept simple\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.0\n"

#: jquery_wrt.admin.inc:16
msgid "Preserve classes"
msgstr "Tabellenklassen erhalten"

#: jquery_wrt.admin.inc:17
msgid "Keep table components classes as far as possible for the responsive output."
msgstr ""
"Klassen-Attributte der Tabellenbestandteile soweit wie möglich in der "
"responsive-Ausgabe erhalten."

#: jquery_wrt.admin.inc:22
msgid "Dynamic table display switching"
msgstr "Dynamischer Tabellendarstellungs-Wechsel"

#: jquery_wrt.admin.inc:23
msgid "TRUE: Toggle table style if settings.dynamicSwitch() returns true. FALSE: Only convert to mobile (one way)"
msgstr ""
"AKTIV: Umschaltung des Tabellenstils ermöglichen, wenn "
"settings.dynamicSwitch() \"true\" zurückliefert. INAKTIV: Nur zur mobilversion "
"wechseln (Einweg)"

#: jquery_wrt.admin.inc:28
msgid "Show switch button"
msgstr "Umschalter anzeigen"

#: jquery_wrt.admin.inc:29
msgid "(Only used if dynamic!) Display a link to switch back from responsive version to original table version."
msgstr ""
"(Nur wenn dynamisch aktiviert!) Einen Link anzeigen, um von der responsive "
"Darstellung zur herkömmlichen Tabellendarstellung zu wechseln."

#: jquery_wrt.admin.inc:39
msgid "Dynamic breakpoint width (px)"
msgstr "Breite für dynamischen Umbruch (in Pixel)"

#: jquery_wrt.admin.inc:42
msgid "If dynamic display is disabled and the default \"displayResponsiveCallback\"-Callback is used (see API Doc), then the mobile display variant is used if the screen width is less than this value (value in px). Default: 960"
msgstr ""
"Wenn die dynamische Darstellung deaktiviert ist und das Standard "
"\"displayResponsiveCallback\"-Callback verwendet wird (siehe API-"
"Dokumentation), dann wird die responsive Darstellung automatisch verwendet, "
"wenn die Darstellungsbreite unter diesem Wert liegt (in Pixel). Standard: "
"'960'."

#: jquery_wrt.admin.inc:53
msgid "JQuery subselector"
msgstr "JQuery subselektor"

#: jquery_wrt.admin.inc:54
msgid "You may optionally use this subselector to reduce the '&lt;table&gt;' DOM context which to use jQuery webks responsive table on. For example add '.my-table' to only use for tables with this class. Uses jQuery \".is()\" selector. Default: \":not(.sticky-header):not(.field-multiple-table):not(:has(tr.draggable))\""
msgstr ""
"Sie können diesen Subselektor nutzen, um den '&lt;table&gt;' DOM Kontext "
"weiter einzuschränken, auf den jQuery webks responsive tables angewandt "
"wird. Ergänzen Sie beispielsweise '.my-table', um die Anwendung nur auf "
"Tabellen mit diesem Klassen-Attribut zu beschränken. Dies entspricht dem "
"jQuery Selektor \".is()\". Standard: \":not(.sticky-header):not(.field-"
"multiple-table):not(:has(tr.draggable))\""

#: jquery_wrt.admin.inc:62
msgid "Header column selector"
msgstr "Kopfzeilen Selektor"

#: jquery_wrt.admin.inc:63
msgid "The header columns selector. Default: 'thead td, thead th' other examples: 'tr th', ..."
msgstr ""
"Der Kopfzeilen/-spalten Selektor. Standard: 'thead td, thead th'. Andere "
"Beispiele: 'tr th', ..."

#: jquery_wrt.admin.inc:70
msgid "Body row selector"
msgstr "Inhaltszeilen Selektor"

#: jquery_wrt.admin.inc:71
msgid "The body rows selector. Default: 'tbody tr'; Other examples: 'tr', ..."
msgstr ""
"Der Inhalszeilen-Selektor. Standard: 'tbody tr'; Andere Beispiele: 'tr', ..."

#: jquery_wrt.admin.inc:79
msgid "Responsive row element"
msgstr "Responsive Reihenelement"

#: jquery_wrt.admin.inc:80
msgid "The responsive rows container element. Default: '&lt;dl>&lt;/dl&gt;'; Other examples: '&lt;ul&gt;&lt;/ul&gt;'. IMPORTANT: Filtered by defaults of filter_xss()."
msgstr ""
"Das Responsive Zeilen Containerelement. Standard: '&lt;dl&gt;&lt;/dl&gt;'; "
"Andere Beispiele: '&lt;ul&gt;&lt;/ul&gt;'. WICHTIG: Wird durch die Drupal "
"filter_xss() Funktion (mit Standardparamentern) gefiltert."

#: jquery_wrt.admin.inc:87
msgid "Responsive column title element"
msgstr "Responsive Spalten Titel Element"

#: jquery_wrt.admin.inc:88
msgid "The responsive column title container element. Default: '&lt;dt&gt;&lt;/dt&gt;'; Other examples: '&lt;li&gt;&lt;/li&gt;'. IMPORTANT: Filtered by defaults of filter_xss()."
msgstr ""
"Das Responsive Spalten Containerelement. Standard: '&lt;dt&gt;&lt;/dt&gt;'; "
"Andere Beispiele: '&lt;li&gt;&lt;/li&gt;'. WICHTIG: Wird durch die Drupal "
"filter_xss() Funktion (mit Standardparamentern) gefiltert."

#: jquery_wrt.admin.inc:95
msgid "Responsive column value element"
msgstr "Responsive Spalten Werte Element"

#: jquery_wrt.admin.inc:96
msgid "The responsive column value container element. Default: '&lt;dt&gt;&lt;/dt&gt;'; Other examples: '&lt;li&gt;&lt;/li&gt;'. IMPORTANT: Filtered by defaults of filter_xss()."
msgstr ""
"Das Responsive Spalten Containerelement. Standard: '&lt;dt&gt;&lt;/dt&gt;'; "
"Andere Beispiele: '&lt;li&gt;&lt;/li&gt;'. WICHTIG: Wird durch die Drupal "
"filter_xss() Funktion (mit Standardparamentern) gefiltert."

#: jquery_wrt.admin.inc:103
msgid "EXTRA: Update on resize"
msgstr "EXTRA: Bei Größenänderung aktualisieren"

#: jquery_wrt.admin.inc:104
msgid "Re-Check display on window resize. Might be buggy in some browsers."
msgstr ""
"Darstellung bei window.resize erneut prüfen. Kann in einigen Browsern zu "
"Fehlern führen."

#: jquery_wrt.module:38
msgid "Adds \"jQuery Plugin webks: Responsive Tables\" (Project: !project, GitHub: !github) to table views in Drupal. Query \"webks Responsive Table\" Plugin transforms less mobile compliant default HTML Tables into a flexible responsive (list) format. Furthermore it provides some nice configuration options to optimize it for your custom needs."
msgstr ""
"Fügt das \"jQuery Plugin webks: Responsive Tables\" (Projekt: !project, "
"GitHub: !github) den Tabellendarstellungen in Drupal hinzu. Das jQuery "
"\"webks Responsive Table\"-Plugin transformiert kaum mobilgeräte-kompatible "
"Standard HTML Tabellen in ein flexibles responsive Listenformat um. Des "
"weiteren bietet es hilfreiche Konfigurationsmöglichkeiten, um es an Ihre "
"Bedürfnisse anzupassen."

#: jquery_wrt.module:42;45
msgid "https://github.com/JPustkuchen/jquery.webks-responsive-table"
msgstr "https://github.com/JPustkuchen/jquery.webks-responsive-table"

#: jquery_wrt.module:55 jquery_wrt.info:0
msgid "jQuery webks: Responsive Tables"
msgstr "jQuery webks: Responsive Tables"

#: jquery_wrt.module:56
msgid "Configure jQuery Plugin webks: Responsive Tables settings."
msgstr "jQuery Plugin webks: Responsive Tables settings konfigurieren"

#: jquery_wrt.info:0
msgid "jQuery \"webks Responsive Table\" Plugin transforms less mobile compliant default HTML Tables into a flexible responsive (list) format. Furthermore it provides some nice configuration options to optimize it for your custom needs."
msgstr ""
"Das jQuery \"webks Responsive Table\"-Plugin transformiert kaum mobilgeräte-"
"kompatible Standard HTML Tabellen in ein flexibles responsive Listenformat "
"um. Des weiteren bietet es hilfreiche Konfigurationsmöglichkeiten, um es an "
"Ihre Bedürfnisse anzupassen."

#: jquery_wrt.info:0
msgid "User interface"
msgstr "Benutzeroberfläche"

#: jquery_wrt.js:0
msgid "Switch to default table view"
msgstr "Standard-Tabellendarstellung verwenden"

#: jquery_wrt.admin.inc:0
msgid "Responsive tables will not be used for paths that match these patterns."
msgstr "Responsive Tables wird für die angegebenen Pfad-Muster ausgeschlossen."